# Distributed TRACLUS using Apache Spark

This repository contains code for a distributed implementation of TRACLUS algorithm (Lee et. al, 2008) using Apache Spark and pyspark. 

The implementation is based on Alex Polcyn's implementation of the TRACLUS algorithm in Python (https://github.com/apolcyn/traclus_impl).

For spatial indexing, the ```pyrtree``` package was used (https://github.com/Rhoana/pyrtree) with a slight modification. The modified version of the package can be found in the ```external/pyrtree``` directory of the repository.

## Distributed TRACLUS implementations

* Random partitioning (branch: ```pyspark_random_partitioning```)
* Spatial partitioning (branch: ```pyspark_spatial_partitioning```)


<!-- ## Utilities

### Converters
* csv2json.py
  Takes as input a CSV file containing the vessel trajectory points and returns a JSON file for use in the traclus_impl scripts.
  
  Example: ```python3 csv2json.py -i ~/Downloads/AIS_2021_02_01.csv -o ../datasets/ais_50_traj.csv -k 50```
  or ```python3 csv2json.py --ifile ~/Downloads/AIS_2021_02_01.csv --ofile ../datasets/ais_50_traj.csv -trajectories_to_keep 50```

* set_parameters.py
  Takes as imput a JSON dataset file and changes its parameters based on the command line arguments given. Can save the results in the same or another file.
  
  Example: ```python3 set_parameters.py -i input_file.json -o output_file.json -e 0.016 -n 3```
  or ```python3 set_parameters.py --ifile input_file.json --ofile output_file.json --epsilon 0.016 --min_neighbors 3```

* json2geojson.py
  Takes as input a JSON file (dataset or result file) and returns geoJSON geometries of the spatial entities contained in the JSON file.
  
  




### Clustering significance
python3 significance/cluster_significance.py -i results/ais_clean_50.json -o significance/ais_clean_50_new.json









python3 traclus/traclus_impl/main.py -i datasets/raw_campus_trajectories.json -o results/raw_campus_trajectories.json -p results/partitions_raw_campus_trajectories.json -c results/clusters_raw_campus_trajectories.json


time spark-submit --master local[*] traclus/traclus_impl/main.py -i datasets/raw_campus_trajectories.json -o results/raw_campus_trajectories.json -p results/partitions_raw_campus_trajectories.json -c results/clusters_raw_campus_trajectories.json


time spark-submit --master local[10] traclus/traclus_impl/main.py -i datasets/ais_clean_200.json -o results/ais_clean_200_original.json -p results/partitions_ais_clean_200.json -c results/clusters_ais_clean_200.json





time spark-submit --master local[10] traclus/traclus_impl/main.py -i datasets/ais_50_traj.json -o results/ais_50_traj.json -p results/ais_50_traj_partitions.json -c results/ais_50_clusters.json -n 10
 -->
