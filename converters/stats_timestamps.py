#!/opt/anaconda/bin/python

from os import linesep
import sys
import getopt
import json
import datetime


def main(argv):
    help_message = ""

    try:
        opts, args = getopt.getopt(argv,"hi:d:",["ifile=","data_type="])
    except getopt.GetoptError:  
        print("\nError in command line arguments.\n" + help_message)
        sys.exit(2)

    # read script input arguments
    for opt, arg in opts:
        # file parameters
        if opt in ("-h", "--help"):
            print(help_message)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            global_inputfile = arg
        elif opt in ("-d", "--data_type"):
            global_datatype = arg
        
    try:
        with open(global_inputfile, 'r') as input_stream:
            parsed_input = json.loads(input_stream.read())
    except:
        print("\nError while reading file.\n", sys.exc_info()[0])
        sys.exit(2)


    for run_stats in parsed_input:
        print("RUN FOR TRAJECTORIES: " + str(run_stats["num_trajectories"]) + "\n----------------------------")

        print_stat("total", run_stats["execution_start"], run_stats["execution_end"])
        print_stat("clean input", run_stats["clean_input_start"], run_stats["clean_input_end"])
        print_stat("partitioning", run_stats["partitioning_start"], run_stats["partitioning_end"])

        if global_datatype == "single":
            print_stat("spatial index creation", run_stats["spatial_index_creation_start"], run_stats["spatial_index_creation_end"])  
            print("spatial index size: " + str(run_stats["spatial_index_size"]))
            print("clusters found: "+str(run_stats["clusters_found"]))

        elif global_datatype == "spatial":
            print_stat("spatial index creation", run_stats["spatial_index_creation_start"], run_stats["spatial_index_creation_end"])
            print_stat("spatial partitioning", run_stats["spatial_partitioning_start"], run_stats["spatial_partitioning_end"])
            print_stat("merging", run_stats["merging_start"], run_stats["merging_end"])
            print("spatial index size: " + str(run_stats["spatial_index_size"]))

        elif global_datatype == "random":
            print_stat("merging", run_stats["merging_start"], run_stats["merging_end"])

        print_stat("clustering", run_stats["clustering_start"], run_stats["clustering_end"])
        print_stat("representatives generation", run_stats["representatives_generation_start"], run_stats["representatives_generation_end"])

        print("representatives generated: "+str(run_stats["representatives_generated"]))
        print("----------------------------\n\n")


def print_stat(name, timestamp_start, timestamp_end):
    start = datetime.datetime.fromtimestamp(timestamp_start)
    end = datetime.datetime.fromtimestamp(timestamp_end)
    delta = end - start
    print(name + " runtime (sec): " + str(delta.seconds))


if __name__ == "__main__":
    main(sys.argv[1:])
