#!/opt/anaconda/bin/python

import sys
import getopt
import json


def main(argv):
    # variable initialization
    # file parameters
    global_inputfile = "dataset.json"
    global_outputfile = "dataset_parameterized.json"

    # TRACLUS parameters
    global_epsilon = 0.016
    global_min_neighbors = 2
    global_min_num_trajectories_in_cluster = 3
    global_min_vertical_lines = 2
    global_min_prev_dist = 0.0002

    help_message = "Available arguments (parentheses contain default values):\n        -i --ifile    Input_file (dataset.csv)\n        -o --ofile    Output file (dataset.json)>\n        -e --epsilon    TRACLUS epsilon parameter (0.00016)\n        -n --min_neighbors    TRACLUS neighbors parameter (2)\n        -c --min_num_trajectories_in_cluster    TRACLUS min number of trajectories in cluster parameter (3)\n        -v --min_vertical_lines    TRACLUS min number of vertical lines parameter (2)\n        -d --min_prev_dist    TRACLUS min previous distance parameter (0.0002)"

    try:
        opts, args = getopt.getopt(argv,"hi:o:e:n:c:v:d:",["ifile=","ofile=","epsilon=","min_neighbors=","min_num_trajectories_in_cluster=","min_vertical_lines=","min_prev_dist="])
    except getopt.GetoptError:  
        print("\nError in command line arguments.\n" + help_message)
        sys.exit(2)

    # read script input arguments
    for opt, arg in opts:
        # file parameters
        if opt in ("-h", "--help"):
            print(help_message)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            global_inputfile = arg
        elif opt in ("-o", "--ofile"):
            global_outputfile = arg

        # TRACLUS parameters
        elif opt in ("-e", "--epsilon"):
            global_epsilon = float(arg)
        elif opt in ("-n", "--min_neighbors"):
            global_min_neighbors = int(arg)
        elif opt in ("-c", "--min_num_trajectories_in_cluster"):
            global_min_num_trajectories_in_cluster = int(arg)
        elif opt in ("-v", "--min_vertical_lines"):
            global_min_vertical_lines = int(arg)
        elif opt in ("-d", "--min_prev_dist"):
            global_min_prev_dist = float(arg)


    with open(global_inputfile) as f:
        data = json.load(f)

    data["epsilon"] = global_epsilon
    data["min_neighbors"] = global_min_neighbors
    data["min_num_trajectories_in_cluster"] = global_min_num_trajectories_in_cluster
    data["min_vertical_lines"] = global_min_vertical_lines
    data["min_prev_dist"] = global_min_prev_dist

    f = open(global_outputfile, "w")
    f.write(json.dumps(data))
    f.close()


if __name__ == "__main__":
    main(sys.argv[1:])
